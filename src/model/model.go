package model

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

//Model ...
type Model struct {
	client *mongo.Client
}

//New ...
func New() *Model {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer func() {
		log.Print("\n********************")
		cancel()
	}()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		/*if err1 := client.Disconnect(ctx); err1 != nil {
			panic(err1)
		}
		return nil*/
		log.Fatal("MongoDB client error")
	}
	errPing := client.Ping(ctx, readpref.Primary())
	if errPing != nil {
		log.Fatal("MongoDB connection error")
	}
	return &Model{
		client: client,
	}
}

//Open ...
func (m *Model) Open() error {
	return nil
}

//Close ...
func (m *Model) Close() {
}
