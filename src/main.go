package main

import (
	"fmt"
	"log"

	"bitbucket.org/yuris11/gowarm/src/config"
	"bitbucket.org/yuris11/gowarm/src/warmserver"
)

func main() {
	fmt.Println("-----------------")
	if err := config.ReadConfig(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Config:", config.MyConfig)
	server := warmserver.New()
	err := server.Start()
	if err != nil {
		log.Fatal(err)
	}
}
