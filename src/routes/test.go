package routes

import (
	"io"
	"net/http"

	"github.com/gorilla/mux"
)

//test_routes ...
func СonfigureTestRoute(r *mux.Router) {
	r.HandleFunc("/", rootRoute())
	r.HandleFunc("/test", testRoute())
}

func rootRoute() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "root")
	}
}

func testRoute() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "test")
	}
}
