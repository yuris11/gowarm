package warmserver

import (
	"net/http"

	"bitbucket.org/yuris11/gowarm/src/config"
	"bitbucket.org/yuris11/gowarm/src/model"
	"bitbucket.org/yuris11/gowarm/src/routes"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

//WarmServer ...
type WarmServer struct {
	router *mux.Router
	model  *model.Model
	logger *logrus.Logger
}

//New ...
func New() *WarmServer {
	router := mux.NewRouter()
	return &WarmServer{
		router: router,
	}
}

func (s *WarmServer) configureRouter() {
	routes.СonfigureTestRoute(s.router)
}

func (s *WarmServer) configureModel() {
	m := model.New()
	s.model = m
}

func (s *WarmServer) configureLogger() error {
	s.logger = logrus.New()
	level, err := logrus.ParseLevel(config.MyConfig.LogLevel)
	if err != nil {
		return err
	}
	s.logger.SetLevel(level)
	return nil
}

//Start ...
func (s *WarmServer) Start() error {
	s.configureRouter()
	s.configureModel()
	if err := s.configureLogger(); err != nil {
		return err
	}
	s.logger.Info("WarmServer started...")
	return http.ListenAndServe(config.MyConfig.Port, s.router)
}
