package config

import (
	"flag"

	"github.com/BurntSushi/toml"
)

//Config ...
type tconfig struct {
	Port     string
	LogLevel string
	Man      tman
}

type tman struct {
	Name string
	Age  int
}

//MyConfig ...
var MyConfig tconfig = tconfig{
	Port:     "5000",
	LogLevel: "debug",
	Man: tman{
		Name: "xxxx",
		Age:  123,
	},
}

var configPath string

//ReadConfig ...
func ReadConfig() error {
	flag.StringVar(&configPath, "c", "./config.toml", "path to config file")
	flag.Parse()
	_, err := toml.DecodeFile(configPath, &MyConfig)
	return err
}
